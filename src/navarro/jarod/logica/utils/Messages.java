package navarro.jarod.logica.utils;

public class Messages {
    public static final String MNSJ_EXITO = "Operación ejecutada exitosamente";
    public static final String MNSJ_REG_REPETIDO = "El registro ya existe";
}
