package navarro.jarod.logica.bl.dao.factory;

import navarro.jarod.logica.bl.dao.cliente.IClienteDao;
import navarro.jarod.logica.bl.dao.cliente.SqlServerClienteDao;
import navarro.jarod.logica.bl.dao.producto.IProductoDao;
import navarro.jarod.logica.bl.dao.producto.SqlServerProductoDao;

public class SqlServerDaoFactory extends DaoFactory {
    public IClienteDao getClienteDao(){
        return new SqlServerClienteDao();
    }
    public IProductoDao getProductoDao(){
        return new SqlServerProductoDao();
    }
}
