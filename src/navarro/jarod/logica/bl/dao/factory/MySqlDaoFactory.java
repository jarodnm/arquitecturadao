package navarro.jarod.logica.bl.dao.factory;

import navarro.jarod.logica.bl.dao.cliente.IClienteDao;
import navarro.jarod.logica.bl.dao.cliente.MySqlClienteDao;
import navarro.jarod.logica.bl.dao.producto.IProductoDao;
import navarro.jarod.logica.bl.dao.producto.MySqlProductoDao;


public class MySqlDaoFactory extends DaoFactory {
    public IClienteDao getClienteDao(){
        return new MySqlClienteDao();
    }
    public IProductoDao getProductoDao() {
        return new MySqlProductoDao();
    }
}
