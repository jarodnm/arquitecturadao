package navarro.jarod.logica.bl.dao.factory;

import navarro.jarod.logica.bl.dao.cliente.IClienteDao;
import navarro.jarod.logica.bl.dao.producto.IProductoDao;

public abstract class DaoFactory {
        public static final int MYSQL = 1 ;
        public static final int SQLSERVER = 2;

        public static DaoFactory getDaoFactory (int factory){
            switch (factory){
                case MYSQL:
                    return new MySqlDaoFactory();
                case SQLSERVER:
                    return new SqlServerDaoFactory();
                default:
                    return null;
            }
        }
        public abstract IClienteDao getClienteDao();
        public abstract IProductoDao getProductoDao();
    }

