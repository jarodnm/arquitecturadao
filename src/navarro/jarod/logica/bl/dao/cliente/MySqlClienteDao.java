package navarro.jarod.logica.bl.dao.cliente;

import navarro.jarod.accesobd.Conector;

import java.util.ArrayList;

public class MySqlClienteDao implements IClienteDao{
        public void registrarCliente(String nombreCompleto, String cedula, int edad, String direccion)throws java.sql.SQLException,Exception {
            java.sql.ResultSet rs;
            String sql;
            sql = "INSERT INTO CLIENTE(NOMBRE,CEDULA,EDAD,DIRECCION) VALUES" + "('"+nombreCompleto+"','"+cedula+"',"+edad+",'"+direccion+"')";
            Conector.getConnector().ejecutarSql(sql);
        }
        public void editarCliente(String cedula, String nombreCompleto, int edad, String direccion)throws java.sql.SQLException,Exception {
            java.sql.ResultSet rs;
            String sql;
            sql = "UPDATE CLIENTE SET NOMBRE = '"+nombreCompleto+ "', EDAD = "+edad+ ",DIRECCION = '"+direccion+"' WHERE CEDULA = '"+cedula+"'";
            Conector.getConnector().ejecutarSql(sql);
        }
        public void eliminarCliente(String cedula)throws java.sql.SQLException,Exception{
            java.sql.ResultSet rs;
            String sql;
            sql = "DELETE FROM CLIENTE WHERE CEDULA = '"+cedula+"'";
            Conector.getConnector().ejecutarSql(sql);
        }

    public ArrayList<Cliente> listarClientes()throws java.sql.SQLException,Exception{
        java.sql.ResultSet rs;
        ArrayList<Cliente> clientes = new ArrayList<>();
        String sql;
        sql = "SELECT * FROM CLIENTE";
        rs = Conector.getConnector().ejecutarQuery(sql);
        while (rs.next()){
            Cliente c = new Cliente();
            c.setNombreCompleto(rs.getString("NOMBRE"));
            c.setCedula(rs.getString("CEDULA"));
            c.setEdad(rs.getInt("EDAD"));
            c.setDireccion(rs.getString("DIRECCION"));
            clientes.add(c);

        }
        return clientes;
    }
}
