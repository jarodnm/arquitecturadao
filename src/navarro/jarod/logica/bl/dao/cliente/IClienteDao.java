package navarro.jarod.logica.bl.dao.cliente;

import java.util.ArrayList;

public interface IClienteDao {
    public void registrarCliente(String nombreCompleto, String cedula, int edad, String direccion)throws java.sql.SQLException,Exception;
    public void editarCliente(String cedula, String nombreCompleto, int edad, String direccion)throws java.sql.SQLException,Exception;
    public void eliminarCliente(String cedula)throws java.sql.SQLException,Exception;
    public ArrayList<Cliente> listarClientes()throws java.sql.SQLException,Exception;
}
