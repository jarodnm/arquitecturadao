package navarro.jarod.logica.bl.dao.producto;

import java.util.ArrayList;

public interface IProductoDao {
    public void registrarProducto(String codigo, String descripcion, double precio, String categoria)throws java.sql.SQLException, Exception;
    public ArrayList<Producto> listarProductos()throws java.sql.SQLException, Exception;
    public void modificarProducto(String codigo, String descripcion, double precio, String categoria)throws java.sql.SQLException, Exception;
    public void eliminarProducto(String codigo)throws java.sql.SQLException, Exception;
}
