package navarro.jarod.logica.bl.dao.producto;

import navarro.jarod.accesobd.Conector;

import java.util.ArrayList;

public class MySqlProductoDao implements IProductoDao {
    public void registrarProducto(String codigo, String descripcion, double precio, String categoria)throws java.sql.SQLException, Exception{
        java.sql.ResultSet rs;
        String sql;
        sql = "INSERT INTO PRODUCTO(CODIGO,DESCRIPCION,PRECIO,CATEGORIA) VALUES" + "('"+codigo+"','"+descripcion+"',"+precio+",'"+categoria+"')";
        Conector.getConnector().ejecutarSql(sql);
    }

    public void modificarProducto(String codigo, String descripcion, double precio, String categoria)throws java.sql.SQLException, Exception{
        java.sql.ResultSet rs;
        String sql;
        sql = "UPDATE PRODUCTO SET CODIGO = '"+codigo+"',DESCRIPCION = '"+descripcion+"', PRECIO = "+precio+", CATEGORIA = '"+categoria+"'";
        Conector.getConnector().ejecutarSql(sql);
    }

    public void eliminarProducto(String codigo)throws java.sql.SQLException, Exception{
        java.sql.ResultSet rs;
        String sql;
        sql = "DELETE FROM PRODUCTO WHERE CODIGO ='" + codigo+ "'";
        Conector.getConnector().ejecutarSql(sql);
    }

    public ArrayList<Producto> listarProductos()throws java.sql.SQLException, Exception{
        java.sql.ResultSet rs;
        String sql;
        sql = "SELECT * FROM PRODUCTO";
        rs = Conector.getConnector().ejecutarQuery(sql);
        ArrayList<Producto> productos = new ArrayList<>();
        while(rs.next()){
            Producto p = new Producto();
            p.setCodigo(rs.getString("CODIGO"));
            p.setDescripcion(rs.getString("DESCRIPCION"));
            p.setPrecio(rs.getDouble("PRECIO"));
            p.setCategoria(rs.getString("CATEGORIA"));
            productos.add(p);
        }
        return productos;
    }
}
