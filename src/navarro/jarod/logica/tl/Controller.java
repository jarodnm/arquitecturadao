package navarro.jarod.logica.tl;

import navarro.jarod.logica.bl.dao.cliente.Cliente;
import navarro.jarod.logica.bl.dao.factory.DaoFactory;
import navarro.jarod.logica.bl.dao.producto.Producto;
import navarro.jarod.logica.exception.*;
import java.sql.SQLException;
import java.util.ArrayList;

public class Controller {
    public void registrarCliente(String nombreCompleto, String cedula, int edad, String direccion)throws MiTiendaException{
        try{
            DaoFactory factory = DaoFactory.getDaoFactory(2);
            factory.getClienteDao().registrarCliente(nombreCompleto,cedula,edad,direccion);
        }catch (SQLException e){

            MiTiendaException tienda = new MiTiendaException( e.getErrorCode());
            throw tienda;
        }
        catch (Exception e){
            MiTiendaException exc = new MiTiendaException(e.getMessage());
        }

    }

    public void editarCliente(String cedula, String nombreCompleto, int edad , String direccion)throws Exception{
        DaoFactory factory = DaoFactory.getDaoFactory(2);
        factory.getClienteDao().editarCliente(cedula,nombreCompleto,edad,direccion);
    }

    public void eliminarCliente(String cedula)throws Exception{
        DaoFactory factory = DaoFactory.getDaoFactory(2);
        factory.getClienteDao().eliminarCliente(cedula);
    }

    public String[] listarClientes ( )throws Exception{

        DaoFactory factory = DaoFactory.getDaoFactory(2);
        String clientes[] = new String[factory.getClienteDao().listarClientes().size()];
        int pos = 0;
        for(Cliente dato:factory.getClienteDao().listarClientes()){
            clientes[pos] = dato.toString();
            pos++;
        }

        return clientes;

    }

    public void registrarProducto(String codigo, String descripcion, double precio, String categoria)throws Exception{
        DaoFactory factory = DaoFactory.getDaoFactory(2);
        factory.getProductoDao().registrarProducto(codigo,descripcion,precio,categoria);
    }

    public void editarProducto(String codigo, String descripcion, double precio, String categoria)throws Exception{
        DaoFactory factory = DaoFactory.getDaoFactory(2);
        factory.getProductoDao().modificarProducto(codigo,descripcion,precio,categoria);
    }

    public void eliminarProducto(String codigo)throws Exception{
        DaoFactory factory = DaoFactory.getDaoFactory(2);
        factory.getProductoDao().eliminarProducto(codigo);
    }

    public String [] listarProductos ()throws Exception{
        DaoFactory factory = DaoFactory.getDaoFactory(2);
        String datos [] = new String[factory.getProductoDao().listarProductos().size()];
        int pos = 0;
        for(Producto dato:factory.getProductoDao().listarProductos()){
            datos[pos]=dato.toString();
            pos++;
        }
        return datos;
    }
}
