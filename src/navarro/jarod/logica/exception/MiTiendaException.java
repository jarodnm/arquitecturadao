package navarro.jarod.logica.exception;

import navarro.jarod.logica.utils.*;

public class MiTiendaException extends Exception {

    private int numero;

    public MiTiendaException() {
        super();
    }

    public MiTiendaException(String message) {
        super(message);
    }

    public MiTiendaException(int numero){
        this.numero = numero;
    }

    public String numeroToString(){
        switch (numero){
            case 2627:
                return Messages.MNSJ_REG_REPETIDO;
            default:
                return Messages.MNSJ_EXITO;
        }
    }
}
